"""Secure random password generation"""

import cracklib
import argparse
from passwords import words

_MAX_WEAK_PWORDS = 100
_MIN_WORD_LENGTH = 4
_MIN_NUM_WORDS = 2


class Generator:
    """Generates secure random passwords"""

    def __init__(self, min_length=20, max_length=50, delimiter='-'):
        self._min_num_words = _MIN_NUM_WORDS
        self._min_word_len = _MIN_WORD_LENGTH
        self._min_len = min_length
        self._max_len = max_length
        self._delimiter = delimiter
        self._words = []
        self._selector = words.Selector()

    def generate(self):
        """Generates a password"""
        self._words = []
        while self._needs_another_word():
            self._words.append(self._next_word)
        if self._length > self._max_len:
            raise OverflowError("Generated password is too long")
        return self.generated_password

    def _needs_another_word(self):
        if self._is_too_short() or self._too_few_words():
            return True
        return self._can_fit_another_word()

    def _can_fit_another_word(self):
        if self._min_word_len < self._remaining_length:
            return self._length < self._max_len
        return False

    def _is_too_short(self):
        return self._length < self._min_len

    def _too_few_words(self):
        return self._num_words < self._min_num_words

    @property
    def _length(self):
        return self._length_of_words + self._length_of_delimiters

    @property
    def _num_words(self):
        return len(self._words)

    @property
    def _length_of_words(self):
        return sum(len(s) for s in self._words)

    @property
    def _length_of_delimiters(self):
        return (self._num_words - 1) * len(self._delimiter)

    @property
    def _next_word(self):
        min_length = self._min_word_len
        max_length = self._remaining_length
        return self._selector.select_word(min_length, max_length)

    @property
    def _remaining_length(self):
        return self._max_len - (self._length + len(self._delimiter))

    @property
    def generated_password(self):
        return self._delimiter.join([word.title() for word in self._words])


def main():
    """Parses cli arguments and retuns them as a dict"""
    parser = argparse.ArgumentParser(description='Secure password generator')
    parser.add_argument('-c',
                        '--count',
                        metavar='COUNT',
                        default=10,
                        help='Number of passwords to generate',
                        type=int)
    parser.add_argument('-d',
                        '--delimiter',
                        metavar='DELIM',
                        help='Separator character between words',
                        default='-',
                        type=str)
    parser.add_argument('--min_length',
                        help='Minimum password length',
                        default=30,
                        type=int)
    parser.add_argument('--max_length',
                        help='Maximum password length',
                        default=100,
                        type=int)

    args = vars(parser.parse_args())
    num_pwords = args.pop('count')
    generator = Generator(**args)
    pwords = []
    num_weak_pwords = 0
    while len(pwords) < num_pwords and num_weak_pwords < _MAX_WEAK_PWORDS:
        try:
            pwords.append(cracklib.VeryFascistCheck(generator.generate()))
        except ValueError:
            num_weak_pwords += 1

    if num_weak_pwords >= _MAX_WEAK_PWORDS:
        print(f"Failed to generate {args.count} passwords after attempts")
        return

    # sort passwords by length, longest first
    pwords.sort(key=len, reverse=True)

    for pword in pwords:
        print(f"    ({len(pword)})    '{pword}'")
