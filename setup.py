#!/usr/bin/env python3
######################################################################
# \file
# \author David J. Kessler <david.j.kessler@jpl.nasa.gov>
# \brief
#
# \copyright
# Copyright 2009-2016, by the California Institute of Technology.
# ALL RIGHTS RESERVED.  United States Government Sponsorship
# acknowledged. Any commercial use must be negotiated with the Office
# of Technology Transfer at the California Institute of Technology.
# \copyright
# This software may be subject to U.S. export control laws and
# regulations.  By accepting this document, the user agrees to comply
# with all U.S. export laws and regulations.  User has the
# responsibility to obtain export licenses, or other export authority
# as may be required before exporting such information to foreign
# countries or providing access to foreign persons.
######################################################################

import os

import setuptools


def get_version():
    path = os.path.dirname(os.path.realpath(__file__))
    version_path = os.path.join(path, 'passwords', 'version.py')
    try:
        with open(version_path, 'r') as fp:
            version_text = fp.read()
        version = version_text.split('=')[1].strip().strip("'")
    except IOError:
        version = '?.?.?'

    return version


setuptools.setup(
    name="PasswordGenerator",
    description='Random Password Generator',
    version=get_version(),
    author='David J Kessler',
    author_email='djkessler@me.com',
    url='https://gitlab.com/DJKessler/passwordgenerator',
    install_requires=['cracklib'],
    packages=['passwords'],
    package_data={'': ['wordlist/*.txt']},
    python_requires='>=3',
    entry_points={
        'console_scripts': [
            'generate-password=passwords.generator:main',
        ]
    },
)
